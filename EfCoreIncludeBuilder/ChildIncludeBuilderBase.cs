﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EfCoreIncludeBuilder
{
    public abstract class ChildIncludeBuilderBase
    {
        public List<ChildIncludeBuilderBase> Includes { get; set; } = new List<ChildIncludeBuilderBase>();
        public IQueryable PerformInclude(IQueryable q)
        {
            if (Includes.Count == 0)
            {
                return _include(q);
            }
            else
            {
                foreach (var i in Includes)
                {
                    q = i.PerformInclude(_include(q));
                }
                return q;
            }
        }
        public abstract IQueryable _include(IQueryable q);
    }
}
