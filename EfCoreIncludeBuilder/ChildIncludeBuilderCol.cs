﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EfCoreIncludeBuilder
{
    public class ChildIncludeBuilderCol2<TP, TP2, T> : ChildIncludeBuilder2<TP, ICollection<TP2>, T> where TP2 : class where TP : class where T : class
    {
        public Expression<Func<TP2, T>> ColNav;

        public ChildIncludeBuilderCol2(Expression<Func<TP2, T>> colNav, ChildIncludeBuilder<TP, ICollection<TP2>> parent) : base(null, parent)
        {
            ColNav = colNav;
        }

        public override IQueryable _include(IQueryable q)
        {
            var s = Done._include(q) as IIncludableQueryable<TP, ICollection<TP2>>;
            return s.ThenInclude(ColNav);
        }
    }

    public class ChildIncludeBuilderCol3<TP, TP2, TP3, T> : ChildIncludeBuilder3<TP, TP2, ICollection<TP3>, T> where TP3 : class where TP2 : class where TP : class where T : class
    {
        public Expression<Func<TP3, T>> ColNav;

        public ChildIncludeBuilderCol3(Expression<Func<TP3, T>> colNav, ChildIncludeBuilder2<TP, TP2, ICollection<TP3>> parent) : base(null, parent)
        {
            ColNav = colNav;
        }

        public override IQueryable _include(IQueryable q)
        {
            var s = Done._include(q) as IIncludableQueryable<TP, ICollection<TP3>>;
            return s.ThenInclude(ColNav);
        }
    }

    public class ChildIncludeBuilderCol4<TP, TP2, TP3, TP4, T> : ChildIncludeBuilder4<TP, TP2, TP3, ICollection<TP4>, T> where TP4: class where TP3 : class where TP2 : class where TP : class where T : class
    {
        public Expression<Func<TP4, T>> ColNav;

        public ChildIncludeBuilderCol4(Expression<Func<TP4, T>> colNav, ChildIncludeBuilder3<TP, TP2, TP3, ICollection<TP4>> parent) : base(null, parent)
        {
            ColNav = colNav;
        }

        public override IQueryable _include(IQueryable q)
        {
            var s = Done._include(q) as IIncludableQueryable<TP, ICollection<TP4>>;
            return s.ThenInclude(ColNav);
        }
    }

    public class ChildIncludeBuilderCol5<TP, TP2, TP3, TP4, TP5, T> : ChildIncludeBuilder5<TP, TP2, TP3, TP4, ICollection<TP5>, T> where TP5 : class where TP4 : class where TP3 : class where TP2 : class where TP : class where T : class
    {
        public Expression<Func<TP5, T>> ColNav;

        public ChildIncludeBuilderCol5(Expression<Func<TP5, T>> colNav, ChildIncludeBuilder4<TP, TP2, TP3, TP4, ICollection<TP5>> parent) : base(null, parent)
        {
            ColNav = colNav;
        }

        public override IQueryable _include(IQueryable q)
        {
            var s = Done._include(q) as IIncludableQueryable<TP, ICollection<TP5>>;
            return s.ThenInclude(ColNav);
        }
    }

    public class ChildIncludeBuilderCol6<TP, TP2, TP3, TP4, TP5, TP6, T> : ChildIncludeBuilder6<TP, TP2, TP3, TP4, TP5, ICollection<TP6>, T> where TP6: class where TP5 : class where TP4 : class where TP3 : class where TP2 : class where TP : class where T : class
    {
        public Expression<Func<TP6, T>> ColNav;

        public ChildIncludeBuilderCol6(Expression<Func<TP6, T>> colNav, ChildIncludeBuilder5<TP, TP2, TP3, TP4, TP5, ICollection<TP6>> parent) : base(null, parent)
        {
            ColNav = colNav;
        }

        public override IQueryable _include(IQueryable q)
        {
            var s = Done._include(q) as IIncludableQueryable<TP, ICollection<TP6>>;
            return s.ThenInclude(ColNav);
        }
    }

    public class ChildIncludeBuilderCol7<TP, TP2, TP3, TP4, TP5, TP6, TP7, T> : ChildIncludeBuilder7<TP, TP2, TP3, TP4, TP5, TP7, ICollection<TP6>, T> where TP7: class where TP6 : class where TP5 : class where TP4 : class where TP3 : class where TP2 : class where TP : class where T : class
    {
        public Expression<Func<TP7, T>> ColNav;

        public ChildIncludeBuilderCol7(Expression<Func<TP7, T>> colNav, ChildIncludeBuilder6<TP, TP2, TP3, TP4, TP5, TP6, ICollection<TP7>> parent) : base(null, parent)
        {
            ColNav = colNav;
        }

        public override IQueryable _include(IQueryable q)
        {
            var s = _parent._include(q) as IIncludableQueryable<TP, ICollection<TP7>>;
            return s.ThenInclude(ColNav);
        }
    }
}
