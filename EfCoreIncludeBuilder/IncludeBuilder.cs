﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EfCoreIncludeBuilder
{
    public class IncludeBuilder
    {
        public static IncludeBuilder<TEntity> Create<TEntity>(IQueryable<TEntity> q) where TEntity : class
        {
            return new IncludeBuilder<TEntity>(q);
        }
    }
    public class IncludeBuilder<T> where T: class
    {
        private List<ChildIncludeBuilderBase> Includes { get; set; } = new List<ChildIncludeBuilderBase>();
        private IQueryable<T> _queryable;
        public IncludeBuilder(IQueryable<T> queryable)
        {
            _queryable = queryable;
        }

        public IncludeBuilder<T> Include<TEntity>(Expression<Func<T, TEntity>> nav) where TEntity : class
        {
            Includes.Add(new ChildIncludeBuilder<T, TEntity>(nav, this));
            return this;
        }

        public ChildIncludeBuilder<T, TEntity> IncludeOnEntity<TEntity>(Expression<Func<T, TEntity>> nav) where TEntity : class
        {
            var res = new ChildIncludeBuilder<T, TEntity>(nav, this);
            Includes.Add(res);
            return res;
        }

        public IQueryable<T> Build()
        {
            IQueryable r = _queryable;
            foreach(var i in Includes)
            {
                r = i.PerformInclude(r);
            }
            return r as IQueryable<T>;
        }
    }
}
