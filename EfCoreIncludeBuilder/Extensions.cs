﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace EfCoreIncludeBuilder
{
    public static class Extensions
    {
        public static ChildIncludeBuilderCol2<TP, T, TEntity> IncludeOnEntity<TP, T, TEntity>(this ChildIncludeBuilder<TP, ICollection<T>> b, Expression<Func<T, TEntity>> nav) where TP : class where T : class where TEntity: class
        {
            var res = new ChildIncludeBuilderCol2<TP, T, TEntity>(nav, b);
            b.Includes.Add(res);
            return res;
        }

        public static ChildIncludeBuilder<TP, ICollection<T>> Include<TP, T, TEntity>(this ChildIncludeBuilder<TP, ICollection<T>> b, Expression<Func<T, TEntity>> nav) where TP : class where T : class where TEntity : class
        {
            var res = new ChildIncludeBuilderCol2<TP, T, TEntity>(nav, b);
            b.Includes.Add(res);
            return b;
        }

        public static ChildIncludeBuilderCol3<TP, TP2, T, TEntity> IncludeOnEntity<TP, TP2, T, TEntity>(this ChildIncludeBuilder2<TP, TP2, ICollection<T>> b, Expression<Func<T, TEntity>> nav) where TP2: class where TP : class where T : class where TEntity : class
        {
            var res = new ChildIncludeBuilderCol3<TP, TP2, T, TEntity>(nav, b);
            b.Includes.Add(res);
            return res;
        }

        public static ChildIncludeBuilder2<TP, TP2, ICollection<T>> Include<TP, TP2, T, TEntity>(this ChildIncludeBuilder2<TP, TP2, ICollection<T>> b, Expression<Func<T, TEntity>> nav) where TP2 : class where TP : class where T : class where TEntity : class
        {
            var res = new ChildIncludeBuilderCol3<TP, TP2, T, TEntity>(nav, b);
            b.Includes.Add(res);
            return b;
        }

        public static ChildIncludeBuilderCol4<TP, TP2, TP3, T, TEntity> IncludeOnEntity<TP, TP2, TP3, T, TEntity>(this ChildIncludeBuilder3<TP, TP2, TP3, ICollection<T>> b, Expression<Func<T, TEntity>> nav) where TP3: class where TP2 : class where TP : class where T : class where TEntity : class
        {
            var res = new ChildIncludeBuilderCol4<TP, TP2, TP3, T, TEntity>(nav, b);
            b.Includes.Add(res);
            return res;
        }

        public static ChildIncludeBuilder3<TP, TP2, TP3, ICollection<T>> Include<TP, TP2, TP3, T, TEntity>(this ChildIncludeBuilder3<TP, TP2, TP3, ICollection<T>> b, Expression<Func<T, TEntity>> nav) where TP3: class where TP2 : class where TP : class where T : class where TEntity : class
        {
            var res = new ChildIncludeBuilderCol4<TP, TP2, TP3, T, TEntity>(nav, b);
            b.Includes.Add(res);
            return b;
        }
    }
}
