﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EfCoreIncludeBuilder
{
    public class ChildIncludeBuilder<TP, T> : ChildIncludeBuilderBase where TP : class where T : class
    {
        public ChildIncludeBuilder<TP, T> Include<TEntity>(Expression<Func<T, TEntity>> nav) where TEntity : class
        {
            Includes.Add(new ChildIncludeBuilder2<TP, T, TEntity>(nav, this));
            return this;
        }
        public ChildIncludeBuilder2<TP, T, TEntity> IncludeOnEntity<TEntity>(Expression<Func<T, TEntity>> nav) where TEntity : class
        {
            var res = new ChildIncludeBuilder2<TP, T, TEntity>(nav, this);
            Includes.Add(res);
            return res;
        }
        public IncludeBuilder<TP> Done { get; private set; }
        public Expression<Func<TP, T>> Nav;

        public ChildIncludeBuilder(Expression<Func<TP, T>> nav, IncludeBuilder<TP> parent)
        {
            Nav           = nav;
            Done          = parent;
        }

        public override IQueryable _include(IQueryable q)
        {
            var s = q as IQueryable<TP>;
            return s.Include(Nav);
        }
    }

    public class ChildIncludeBuilder2<TP, TP2, T> : ChildIncludeBuilderBase where TP2 : class where TP : class where T : class
    {
        public ChildIncludeBuilder2<TP, TP2, T> Include<TEntity>(Expression<Func<T, TEntity>> nav) where TEntity : class
        {
            Includes.Add(new ChildIncludeBuilder3<TP, TP2, T, TEntity>(nav, this));
            return this;
        }
        public ChildIncludeBuilder3<TP, TP2, T, TEntity> IncludeOnEntity<TEntity>(Expression<Func<T, TEntity>> nav) where TEntity : class
        {
            var res = new ChildIncludeBuilder3<TP, TP2, T, TEntity>(nav, this);
            Includes.Add(res);
            return res;
        }
        public ChildIncludeBuilder<TP, TP2> Done { get; private set; }
        public Expression<Func<TP2, T>> Nav;

        public ChildIncludeBuilder2(Expression<Func<TP2, T>> nav, ChildIncludeBuilder<TP, TP2> parent)
        {
            Nav = nav;
            Done = parent;
        }

        public override IQueryable _include(IQueryable q)
        {
            var s = Done._include(q) as IIncludableQueryable<TP, TP2>;
            return s.ThenInclude(Nav);
        }
    }

    public class ChildIncludeBuilder3<TP, TP2, TP3, T> : ChildIncludeBuilderBase where TP3 : class where TP2 : class where TP : class where T : class
    {
        public ChildIncludeBuilder3<TP, TP2, TP3, T> Include<TEntity>(Expression<Func<T, TEntity>> nav) where TEntity : class
        {
            var res = new ChildIncludeBuilder4<TP, TP2, TP3, T, TEntity>(nav, this);
            Includes.Add(res);
            return this;
        }
        public ChildIncludeBuilder4<TP, TP2, TP3, T, TEntity> IncludeOnEntity<TEntity>(Expression<Func<T, TEntity>> nav) where TEntity : class
        {
            var res = new ChildIncludeBuilder4<TP, TP2, TP3, T, TEntity>(nav, this);
            Includes.Add(res);
            return res;
        }
        public ChildIncludeBuilder2<TP, TP2, TP3> Done { get; private set; }
        public Expression<Func<TP3, T>> Nav;
        public ChildIncludeBuilder3(Expression<Func<TP3, T>> nav, ChildIncludeBuilder2<TP, TP2, TP3> parent)
        {
            Nav = nav;
            Done = parent;
        }

        public override IQueryable _include(IQueryable q)
        {
            var s = Done._include(q) as IIncludableQueryable<TP, TP3>;
            return s.ThenInclude(Nav);
        }
    }
    public class ChildIncludeBuilder4<TP, TP2, TP3, TP4, T> : ChildIncludeBuilderBase where TP4 : class where TP3 : class where TP2 : class where TP : class where T : class
    {
        public ChildIncludeBuilder4<TP, TP2, TP3, TP4, T> Include<TEntity>(Expression<Func<T, TEntity>> nav) where TEntity : class
        {
            var res = new ChildIncludeBuilder5<TP, TP2, TP3, TP4, T, TEntity>(nav, this);
            Includes.Add(res);
            return this;
        }
        public ChildIncludeBuilder5<TP, TP2, TP3, TP4, T, TEntity> IncludeOnEntity<TEntity>(Expression<Func<T, TEntity>> nav) where TEntity : class
        {
            var res = new ChildIncludeBuilder5<TP, TP2, TP3, TP4, T, TEntity>(nav, this);
            Includes.Add(res);
            return res;
        }
        public ChildIncludeBuilder3<TP, TP2, TP3, TP4> Done { get; private set; }
        public Expression<Func<TP4, T>> Nav;
        public ChildIncludeBuilder4(Expression<Func<TP4, T>> nav, ChildIncludeBuilder3<TP, TP2, TP3, TP4> parent)
        {
            Nav = nav;
            Done = parent;
        }

        public override IQueryable _include(IQueryable q)
        {
            var s = Done._include(q) as IIncludableQueryable<TP, TP4>;
            return s.ThenInclude(Nav);
        }
    }

    public class ChildIncludeBuilder5<TP, TP2, TP3, TP4, TP5, T> : ChildIncludeBuilderBase where TP5 : class where TP4 : class where TP3 : class where TP2 : class where TP : class where T : class
    {
        public ChildIncludeBuilder5<TP, TP2, TP3, TP4, TP5, T> Include<TEntity>(Expression<Func<T, TEntity>> nav) where TEntity : class
        {
            var res = new ChildIncludeBuilder6<TP, TP2, TP3, TP4, TP5, T, TEntity>(nav, this);
            Includes.Add(res);
            return this;
        }
        public ChildIncludeBuilder6<TP, TP2, TP3, TP4, TP5, T, TEntity> IncludeOnEntity<TEntity>(Expression<Func<T, TEntity>> nav) where TEntity : class
        {
            var res = new ChildIncludeBuilder6<TP, TP2, TP3, TP4, TP5, T, TEntity>(nav, this);
            Includes.Add(res);
            return res;
        }
        public ChildIncludeBuilder4<TP, TP2, TP3, TP4, TP5> Done { get; }
        public Expression<Func<TP5, T>> Nav;
        public ChildIncludeBuilder5(Expression<Func<TP5, T>> nav, ChildIncludeBuilder4<TP, TP2, TP3, TP4, TP5> parent)
        {
            Nav = nav;
            Done = parent;
        }

        public override IQueryable _include(IQueryable q)
        {
            var s = Done._include(q) as IIncludableQueryable<TP, TP5>;
            return s.ThenInclude(Nav);
        }

    }
    public class ChildIncludeBuilder6<TP, TP2, TP3, TP4, TP5, TP6, T> : ChildIncludeBuilderBase where TP6 : class where TP5 : class where TP4 : class where TP3 : class where TP2 : class where TP : class where T : class
    {
        public ChildIncludeBuilder6<TP, TP2, TP3, TP4, TP5, TP6, T> Include<TEntity>(Expression<Func<T, TEntity>> nav) where TEntity : class
        {
            var res = new ChildIncludeBuilder7<TP, TP2, TP3, TP4, TP5, TP6, T, TEntity>(nav, this);
            Includes.Add(res);
            return this;
        }

        public ChildIncludeBuilder5<TP, TP2, TP3, TP4, TP5, TP6> Done { get; private set; }
        public Expression<Func<TP6, T>> Nav;

        public ChildIncludeBuilder6(Expression<Func<TP6, T>> nav, ChildIncludeBuilder5<TP, TP2, TP3, TP4, TP5, TP6> parent)
        {
            Nav = nav;
            Done = parent;
        }

        public override IQueryable _include(IQueryable q)
        {
            var s = Done._include(q) as IIncludableQueryable<TP, TP6>;
            return s.ThenInclude(Nav);
        }
    }

    public class ChildIncludeBuilder7<TP, TP2, TP3, TP4, TP5, TP6, TP7, T> : ChildIncludeBuilderBase where TP6 : class where TP5 : class where TP4 : class where TP3 : class where TP2 : class where TP : class where T : class
    {
        public Expression<Func<TP7, T>> Nav;
        protected ChildIncludeBuilderBase _parent;
        public ChildIncludeBuilder7(Expression<Func<TP7, T>> nav, ChildIncludeBuilderBase parent)
        {
            Nav = nav;
            _parent = parent;
        }

        public override IQueryable _include(IQueryable q)
        {
            var s = _parent._include(q) as IIncludableQueryable<TP, TP7>;
            return s.ThenInclude(Nav);
        }
    }
}
